document.addEventListener("DOMContentLoaded", function() {
    const passwordInputs = document.querySelectorAll("input[type='password']");
    const icons = document.querySelectorAll(".icon-password");

icons.forEach((icon, index) => {
      icon.addEventListener("click", () => {
      const passwordInput = passwordInputs[index];
      if (passwordInput) {
          if (passwordInput.type === "password") {
              passwordInput.type = "text";
              icon.classList.remove("fa-eye-slash");
              icon.classList.add("fa-eye");
          } else {
              passwordInput.type = "password";
              icon.classList.remove("fa-eye");
              icon.classList.add("fa-eye-slash");
          }
      }
   });
});

const form = document.querySelector(".password-form");
      form.addEventListener("submit", (event) => {
           event.preventDefault();
           const firstPassword = passwordInputs[0].value;
           const secondPassword = passwordInputs[1].value;
           if (firstPassword === secondPassword) {
           alert("You are welcome");
      } else {
           const errorText = document.createElement("p");
           errorText.textContent = "Потрібно ввести однакові значення";
           errorText.style.color = "red";
           const label = passwordInputs[1].closest(".input-wrapper");
           label.appendChild(errorText);
      }
   });
});